module gitlab.com/opennota/fb2index

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20211113001501-0c823b97ae02 // indirect
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.7 // indirect
	gopkg.in/ini.v1 v1.66.3
	modernc.org/libc v1.11.72 // indirect
	modernc.org/sqlite v1.14.1
)

go 1.16
