// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
	"testing"
	"time"
)

func TestBasicFunctionality(t *testing.T) {
	if err := exec.Command("go", "build").Run(); err != nil {
		t.Fatal(err)
	}
	cmd := exec.Command("./fb2index", "-r", "testdata")
	if err := cmd.Start(); err != nil {
		t.Fatal(err)
	}
	defer cmd.Process.Kill() //nolint:errcheck

	time.Sleep(1 * time.Second)

	// Index page

	resp, err := http.Get("http://localhost:8080/")
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatal("want HTTP OK, got", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatal("got incomplete HTML")
	}
	if !bytes.Contains(data, []byte(`<div class="book">`)) {
		t.Fatal("want a book in the list, got none")
	}

	// Book page

	resp, err = http.Get("http://localhost:8080/b?id=1")
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatal("want HTTP OK, got", resp.StatusCode)
	}
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatal("got incomplete HTML")
	}
	if !bytes.Contains(data, []byte(`<div class="annotation">`)) {
		t.Fatal("want an annotation, got none")
	}
	if !bytes.Contains(data, []byte(`Это тестовый файл FictionBook 2.0`)) {
		t.Fatal("want annotation to contain `Это тестовый файл FictionBook 2.0`")
	}

	// Search

	resp, err = http.Post("http://localhost:8080/search", "application/x-www-form-urlencoded", strings.NewReader("query=войны"))
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatal("want HTTP OK, got", resp.StatusCode)
	}
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(data, []byte("</html>")) {
		t.Fatal("got incomplete HTML")
	}
	if !bytes.Contains(data, []byte(`<div class="book">`)) {
		t.Fatal("want a search result, got none")
	}
	if !bytes.Contains(data, []byte(`<a class="book-link" href="/b?id=1">Война и мир</a>`)) {
		t.Fatal("want a link to the found book, got none")
	}
}
